<?php

/*
|--------------------------------------------------------------------------
| SmsRu configurations
|--------------------------------------------------------------------------
*/


return [

    'timeout'=>30,

    'api_id'=>env('SMSRU_API_ID',null),

    'json'=>1,

    'test_mode'=>(int) env('SMSRU_TEST_MODE', env('APP_DEBUG', 1)),

    'api_url'=>env('SMSRU_API_URL','https://sms.ru/sms/send'),

    'sender'=>env('SMSRU_SENDER', env('APP_NAME')),
];
