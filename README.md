# SmsRuAPI 
## Установка
Установите пакет
`composer require rsoftsteam/sms-ru dev-master`

## Настройка
Добавьте Сервис провайдер в окружение вашего приложения
`config/app.php`

    `'providers' => [
      ...
        /*
         * Package Service Providers...
         */
        SmsRu\SmsRuServiceProvider::class,
      ...
    ]`

Опубликуйте файл конфигурации
`php artisan vendor:publish --provider=SmsRu\SmsRuServiceProvider`

Отредактируйте файл конфигурации `config/smsRu.php` в соответствии с вашими настройками
Достаточно указать ваш `api_id`, получить его можно в личном кабинете http://sms.ru
`'api_id'=>env('SMSRU_API_ID', <ВАШ API_ID>)`
Подробное описание всех параметров указано в файле `config/smsRu.php`

## Использование
Отправка смс осуществляется посредством системы уведомлений Laravel Notifications.
Для этого используется Канал уведомлений `SmsRu\SmsRuChannel`

Создайте новое `php artisan make:notification <Name>` или отредактируйте существующий класс уведомления.

добавьте метод toSmsRu в ваш класс уведомления

    `public function toSmsRu($notifiable)
    {
        return $this->message->to($notifiable->phone);
    }` 

добавьте в список методов доставки уведомлений канал smsRu

    public function via($notifiable)
    {
        return [
        ...
        SmsRuChannel::class
        ];
    }
