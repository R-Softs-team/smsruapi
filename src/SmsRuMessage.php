<?php
/**
 * Created by IntelliJ IDEA.
 * User: rustam
 * Date: 06.02.19
 * Time: 15:57
 */

namespace SmsRu;

class SmsRuMessage
{
    protected $sender;
    protected $to;
    protected $text;

    public function __construct(string $text, ?string $sender = null)
    {
        $this->sender = $sender??config('smsRu.sender');
        $this->text = $text;
    }

    public function sender(?string $sender = null): self
    {
        $this->sender = !empty($sender) ? $sender : null;
        return $this;
    }

    public function to(?string $to = null): self
    {
        $this->to = !empty($to) ? $to : null;
        return $this;
    }

    public function text(?string $text = null): self
    {
        $this->text = !empty($text) ? $text : null;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'from'=>$this->sender,
            'to'=>$this->to,
            'msg'=>$this->text,
        ];
    }

    public function toJson(): ?string
    {
        return json_encode($this->toArray());
    }

    public function validate(): bool
    {
        return !(empty($this->to) || empty($this->text));
    }
}