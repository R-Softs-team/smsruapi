<?php
/**
 * Created by IntelliJ IDEA.
 * User: rustam
 * Date: 11.02.19
 * Time: 16:11
 */

namespace SmsRu\Contracts;


use SmsRu\SmsRuMessage;

interface SmsRu
{
    public function send(SmsRuMessage $message): ?string;
}