<?php

namespace SmsRu;

use SmsRu\Contracts\SmsRu as SmsRuContract;

class SmsRu implements SmsRuContract
{
    protected $conf;


    /**
     * SmsRu constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->conf = $config;
    }

    /**
     * @param SmsRuMessage $message
     * @return string|null
     * @throws \Exception
     */
    public function send(SmsRuMessage $message): ?string
    {
        try {
            if ($message->validate()) {
                $data = [
                    'api_id' => $this->conf['api_id']??null,
                    'json' => $this->conf['json'] ?? 1,
                    'test' => $this->conf['test_mode'] ?? 0,
                ];

                $ch = curl_init($this->conf['api_url']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, $this->conf['timeout']);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array_merge($data, $message->toArray())));
                $body = curl_exec($ch);
                curl_close($ch);

                return $body;
            }
        } catch (\Exception $e) {
            throw new \Exception("SmsRuMessage error validation");
        }
    }
}