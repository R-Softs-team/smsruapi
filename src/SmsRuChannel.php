<?php

namespace SmsRu;

use Illuminate\Notifications\Notification;

class SmsRuChannel
{
    protected $smsRu;

    public function __construct(SmsRu $smsRu)
    {
        $this->smsRu = $smsRu;
    }

    /**
     * @param $notifiable
     * @param Notification $notification
     * @throws \Exception
     */
    public function send($notifiable, Notification $notification)
    {
        $this->smsRu->send($notification->toSmsRu($notifiable));
    }
}