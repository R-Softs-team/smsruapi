<?php
/**
 * Created by IntelliJ IDEA.
 * User: rustam
 * Date: 06.02.19
 * Time: 14:54
 */

namespace SmsRu;

use Illuminate\Support\ServiceProvider;

class SmsRuServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/smsRu.php' => config_path('smsRu.php'),
        ]);
    }

    public function register()
    {
        $this->app->singleton(SmsRu::class, function ($app) {
            return new SmsRu(config('smsRu'));
        });
    }

    public function provides()
    {
        return [ SmsRu::class ];
    }
}